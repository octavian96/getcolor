﻿public enum Color
{
    Empty = 0,
    Red,
    Green,
    Blue,
    Purple,
    Orange,
    Pink,
    Yellow
}
