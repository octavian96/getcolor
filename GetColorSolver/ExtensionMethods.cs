﻿namespace GetColorSolver
{
    internal static class ExtensionMethods
    {
        public static void Add<T>(this Stack<T> stack, T item)
        {
            stack.Push(item);
        }

        public static void Add<T>(this Queue<T> queue, T item)
        {
            queue.Enqueue(item);
        }

        public static bool TryGet<T>(this Stack<T> stack, out T item)
        {
            return stack.TryPop(out item);
        }

        public static bool TryGet<T>(this Queue<T> queue, out T item)
        {
            return queue.TryDequeue(out item);
        }
    }
}
