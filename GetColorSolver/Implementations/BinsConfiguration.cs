﻿using GetColorSolver.Interfaces;
using System.ComponentModel.Design;

namespace GetColorSolver.Implementations
{
    internal class BinsConfiguration : IBinsConfiguration
    {
        private readonly List<IBin> bins;
        public IEnumerable<IBin> Bins
        {
            get
            {
                return bins.ToList();
            }
        }
        public bool IsSolved => Bins.All(b => b.IsComplete);
        private readonly Dictionary<string, IBinsConfiguration> childConfigurations = new();
        public IEnumerable<IBinsConfiguration> ChildConfigurations => childConfigurations.Values.ToList();

        private readonly HashSet<IBinsConfiguration> fatherConfigurations = new();
        public IEnumerable<IBinsConfiguration> FatherConfigurations => fatherConfigurations.ToList();

        public int TreeDepth { get; private set; }

        internal BinsConfiguration()
        {
            bins = new();
        }
        internal BinsConfiguration(IEnumerable<IBin> initialBottles)
        {
            bins = initialBottles/*.OrderBy(b => b)*/.ToList();
        }
        internal BinsConfiguration(IEnumerable<IBin> initialBottles, IBinsConfiguration father) : this(initialBottles)
        {
            fatherConfigurations.Add(father);
            if (father.TreeDepth < TreeDepth)
                TreeDepth = father.TreeDepth;
        }

        public bool AddFather(IBinsConfiguration father)
        {
            if (fatherConfigurations.Add(father))
            {
                if (father.TreeDepth < TreeDepth)
                    TreeDepth = father.TreeDepth;
                return true;
            }
            else
                return false;
        }
        public bool RemoveFather(IBinsConfiguration father)
        {
            if (fatherConfigurations.Remove(father))
            {
                TreeDepth = fatherConfigurations.Min(f => f.TreeDepth);
                return false;
            }
            else
                return false;
        }

        public IEnumerable<IBinsConfiguration> GenerateNextConfigurations(bool force = false)
        {
            if (force)
                childConfigurations.Clear();

            if (childConfigurations.Count > 0)
                return ChildConfigurations;

            for (int bottleIndex = 0; bottleIndex < bins.Count; ++bottleIndex)
            {
                var alreadyTopped = new List<int>();
                for (int colorIndex = bins[bottleIndex].TopColorsCount; colorIndex > 0; --colorIndex)
                {
                    var newBottles = bins.Select(b => new Bottle(b)).ToList();
                    var colors = newBottles[bottleIndex].PopTopColors(colorIndex);
                    for (int newBottleIndex = 0; newBottleIndex < newBottles.Count; ++newBottleIndex)
                    {
                        if (newBottleIndex == bottleIndex)
                            continue;
                        if (newBottles[newBottleIndex].CanAdd(colors) && !alreadyTopped.Contains(newBottleIndex))
                        {
                            alreadyTopped.Add(newBottleIndex);
                            var tmp = newBottles.Select(b => new Bottle(b)).ToList();
                            tmp[newBottleIndex].TryAdd(colors);
                            BinsConfiguration childConfiguration = new(tmp, this);
                            childConfigurations.TryAdd(childConfiguration.ToString(true), childConfiguration);
                        }
                    }
                }
            }

            return ChildConfigurations;
        }

        public void Canonicalize()
        {
            bins.Sort();
        }

        public override string ToString() => string.Join("\n", bins.Select(b => b.ToString()));
        public string ToString(bool canonical)
        {
            if (canonical)
            {
                var canonicBottles = bins.OrderBy(b => b);
                return string.Join("\n", canonicBottles.Select(b => b.ToString()));
            }
            else
                return ToString();
        }

        public void Add(IBin bin)
        {
            bins.Add(bin);
            childConfigurations.Clear();
        }

        public bool AddChild(IBinsConfiguration child)
        {
            if (childConfigurations.ContainsKey(child.ToString(true)))
                return false;
            childConfigurations.Add(child.ToString(true), child);
            child.AddFather(this);
            return true;
        }

        public bool RemoveChild(IBinsConfiguration child)
        {
            if (childConfigurations.ContainsKey(child.ToString(true)))
            {
                childConfigurations.Remove(child.ToString(true));
                child.RemoveFather(this);
                return true;
            }
            else
                return false;
        }
    }
}
