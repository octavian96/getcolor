﻿using GetColorSolver.Interfaces;

namespace GetColorSolver.Implementations
{
    internal class BreadthFirst : BottlePuzzle
    {
        private readonly IBinsConfiguration initialConfiguration;
        public override IBinsConfiguration InitialConfiguration => initialConfiguration;
        private readonly Queue<IBinsConfiguration> configurations = new();
        internal BreadthFirst(IBinsConfiguration initialConfiguration)
        {
            this.initialConfiguration = initialConfiguration;
            configurations.Enqueue(initialConfiguration);
            configurationsTree.Add(initialConfiguration.ToString(true), initialConfiguration);
        }

        protected override bool TryGetNextConfiguration(out IBinsConfiguration? configuration) => configurations.TryDequeue(out configuration);

        protected override void StoreNextConfiguration(IBinsConfiguration configuration) => configurations.Enqueue(configuration);
    }
}
