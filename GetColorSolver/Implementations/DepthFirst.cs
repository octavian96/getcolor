﻿using GetColorSolver.Interfaces;

namespace GetColorSolver.Implementations
{
    internal class DepthFirst : BottlePuzzle
    {
        private readonly IBinsConfiguration initialConfiguration;
        public override IBinsConfiguration InitialConfiguration => initialConfiguration;
        private readonly Stack<IBinsConfiguration> configurations = new();
        internal DepthFirst(IBinsConfiguration initialConfiguration)
        {
            this.initialConfiguration = initialConfiguration;
            configurations.Push(initialConfiguration);
            configurationsTree.Add(initialConfiguration.ToString(true), initialConfiguration);
        }
        protected override bool TryGetNextConfiguration(out IBinsConfiguration? configuration) => configurations.TryPop(out configuration);

        protected override void StoreNextConfiguration(IBinsConfiguration configuration) => configurations.Push(configuration);

    }
}
