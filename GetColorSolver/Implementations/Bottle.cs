﻿using System.Text;
using GetColorSolver.Interfaces;

namespace GetColorSolver.Implementations
{
    internal class Bottle : IComparable, IBin
    {
        private readonly Color[] content;
        public int Capacity => content.Length;
        public IEnumerable<Color> Content => content.ToList();
        public int Level
        {
            get
            {
                var index = Array.IndexOf(content, Color.Empty);
                return index >= 0 ? index : Capacity;
            }
        }
        public int Available => Capacity - Level;
        public Color TopColor => content[Level > 0 ? Level - 1 : Level];
        public int TopColorsCount
        {
            get
            {
                int count = 0;
                for (int i = Level - 1; i >= 0; --i)
                {
                    if (content[i] == TopColor)
                        ++count;
                    else
                        break;
                }
                return count;
            }
        }
        public bool IsFull => Level == Capacity;
        public bool IsComplete => content.All(c => c == TopColor);
        public Color ContentAt(int level) => content[level];

        internal Bottle(int capacity)
        {
            if (capacity < 1)
                throw new ArgumentException("Argument capacity must be greater than 0", paramName: nameof(capacity));

            content = new Color[capacity];
        }

        internal Bottle(int capacity, IEnumerable<Color> colors) : this(capacity)
        {
            colors = colors.Where(c => c != Color.Empty);
            if (colors.Count() > Capacity)
                throw new ArgumentException("Too many colors", paramName: nameof(colors));
            foreach (var color in colors)
                content[Level] = color;
        }

        internal Bottle(IBin bottle) : this(bottle.Capacity, bottle.Content) { }

        public IEnumerable<Color> PopTopColors(int amount = 0)
        {
            if (amount > TopColorsCount)
                throw new ArgumentException($"Cannot get more than {TopColorsCount} colors", paramName: nameof(amount));
            amount = amount > 0 ? amount : TopColorsCount;
            Range range = (Level - amount)..Level;
            var colors = content[range];
            if (colors.Any(c => c != colors[0]))
                colors = Array.Empty<Color>();
            else
            {
                for (int i = amount; i > 0; --i)
                    content[Level - 1] = Color.Empty;
            }
            return colors;
        }
        public IEnumerable<Color> PeekTopColors(int amount = 0)
        {
            if (amount > TopColorsCount)
                throw new ArgumentException($"Cannot peek more than {TopColorsCount} colors", paramName: nameof(amount));
            amount = amount > 0 ? amount : TopColorsCount;
            Range range = (Level - amount)..(Level - 1);
            var colors = content[range];
            if (colors.Any(c => c != colors[0]))
                colors = Array.Empty<Color>();

            return colors;
        }
        public IEnumerable<Color> PeekContent(int amount = 0)
        {
            if (amount > Level)
                throw new ArgumentException($"Cannot peek more than {Level} colors", paramName: nameof(amount));
            amount = amount > 0 ? amount : Level;
            Range range = (Level - amount)..(Level - 1);
            var colors = content[range];

            return colors;
        }

        public bool CanAdd(IEnumerable<Color> colors)
        {
            if (IsFull)
                return false;
            if (colors.Any(c => c == Color.Empty))
                return false;
            if (colors.Any(c => c != colors.First()))
                return false;
            if (colors.First() != TopColor && TopColor != Color.Empty)
                return false;
            if (Available < colors.Count())
                return false;
            return true;
        }

        public bool TryAdd(IEnumerable<Color> colors)
        {
            if (!CanAdd(colors))
                return false;
            foreach (var color in colors)
                content[Level] = color;
            return true;
        }

        public override string ToString() => string.Join("-", content);

        public int CompareTo(object? obj)
        {
            if (obj == null) return 1;

            if (obj is Bottle otherBottle)
                return ToString().CompareTo(otherBottle.ToString());
            else
                throw new ArgumentException("Object is not a Bottle", paramName: nameof(obj));
        }

        private string htmlColorTemplate = @"
<div 
                class=""liquid middle""
                style=""
                    background: COLOR;
                    filter: drop-shadow(0 0 80px COLOR);
                    top: TOP%;
                    bottom: BOTTOM%;""></div>
";
        public string ToHTML()
        {
            var html = new StringBuilder();
            html.Append(@"<div class=""bowl"">");

            int percentageHeight = 100 / Capacity;
            for (int i = 0; i < Level; ++i)
            {
                var color = content[i];
                string colorTemplate = htmlColorTemplate.Replace("COLOR", color.ToString());
                colorTemplate = colorTemplate.Replace("BOTTOM", (i * percentageHeight).ToString());
                colorTemplate = colorTemplate.Replace("TOP", (100 - (i + 1) * percentageHeight).ToString());

                if (i == 0)
                    colorTemplate = colorTemplate.Replace("middle", "bottom");
                if (i == Level - 1)
                    colorTemplate = colorTemplate.Replace("middle", "middle top");

                html.Append(colorTemplate);
            }

            html.Append("</div>");
            return html.ToString();
        }
    }

}
