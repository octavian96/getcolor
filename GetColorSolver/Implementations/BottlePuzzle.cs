﻿using GetColorSolver.Interfaces;

namespace GetColorSolver.Implementations
{
    // Queue - FIFO - Breadth first
    // Stack - LIFO - Depth first
    internal abstract class BottlePuzzle : IPuzzleSolver
    {
        public abstract IBinsConfiguration InitialConfiguration { get; }
        protected readonly Dictionary<string, IBinsConfiguration> configurationsTree = new();

        public IEnumerable<IBinsConfiguration> SolvingConfigurations
        {
            get
            {
                return configurationsTree
                    .Values
                    .Where(c => c.IsSolved);
            }
        }
        public bool IsSolved => configurationsTree.Values.Any(c=>c.IsSolved);

        protected abstract bool TryGetNextConfiguration(out IBinsConfiguration? configuration);
        protected abstract void StoreNextConfiguration(IBinsConfiguration configuration);

        protected virtual IEnumerable<IBinsConfiguration> SelectNextMoves(IEnumerable<IBinsConfiguration> moves)
        {
            List<IBinsConfiguration> filtered = new();
            foreach (var move in moves)
            {
                if (configurationsTree.TryGetValue(move.ToString(true), out IBinsConfiguration configuration))
                {
                    foreach (var father in move.FatherConfigurations)
                        configuration.AddFather(father);
                }
                else
                {
                    configurationsTree.Add(move.ToString(true), move);
                    filtered.Add(move);
                }
            }
            return filtered;
        }

        private bool GenerateNextMoves()
        {
            if (!TryGetNextConfiguration(out var current))
                return false;

            var moves = current.GenerateNextConfigurations();
            moves = SelectNextMoves(moves);
            foreach (var move in moves)
                StoreNextConfiguration(move);
            return true;
        }

        public virtual void Solve()
        {
            while (GenerateNextMoves()) ;
        }

        public IEnumerable<IBinsConfiguration> ConfigurationPath(IBinsConfiguration configuration)
        {
            List<IBinsConfiguration> path = new() { configuration };
            while (configuration.FatherConfigurations.Any())
            {
                configuration = configuration.FatherConfigurations.MinBy(f => f.TreeDepth);
                path.Add(configuration);
            }
            path.Reverse();
            return path;
        }
        public string ConfigurationPathString(IBinsConfiguration configuration)
        {
            var path = ConfigurationPath(configuration);
            return string.Join("\n\n", path);
        }

        public string SolutionPathString()
        {
            if (!IsSolved)
                return string.Empty;
            return ConfigurationPathString(SolvingConfigurations.MinBy(f => f.TreeDepth));
        }

        public IEnumerable<IBinsConfiguration> SolutionPath()
        {
            if (!IsSolved)
                return Array.Empty<IBinsConfiguration>();
            return ConfigurationPath(SolvingConfigurations.MinBy(f => f.TreeDepth));
        }
    }
}