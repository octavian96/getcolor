﻿using GetColorSolver.Implementations;

namespace GetColorSolver.Interfaces
{
    public class PuzzleFactory
    {
        public static IPuzzleSolver DepthFirstPuzzle(IBinsConfiguration initialConfiguration)
        {
            return new DepthFirst(initialConfiguration);
        }

        public static IPuzzleSolver BreadthFirstPuzzle(IBinsConfiguration initialConfiguration)
        {
            return new BreadthFirst(initialConfiguration);
        }

        public static IBin BinFactory(int capacity)
        {
            return new Bottle(capacity);
        }

        public static IBin BinFactory(int capacity, IEnumerable<Color> colors)
        {
            return new Bottle(capacity, colors);
        }

        public static IBinsConfiguration ConfigurationFactory()
        {
            return new BinsConfiguration();
        }

        public static IBinsConfiguration ConfigurationFactory(IEnumerable<IBin> initialBottles)
        {
            return new BinsConfiguration(initialBottles);
        }

        public static IBinsConfiguration ConfigurationFactory(IEnumerable<IBin> initialBottles, IBinsConfiguration father)
        {
            return new BinsConfiguration(initialBottles, father);
        }

        public static IBinsConfiguration ConfigurationFactory(int bottleCapacity, IEnumerable<IEnumerable<Color>> colorsList)
        {
            var bottles = colorsList.Select(colors => new Bottle(bottleCapacity, colors));
            return new BinsConfiguration(bottles);
        }
    }
}
