﻿namespace GetColorSolver.Interfaces
{
    public interface IBinsConfiguration
    {
        public IEnumerable<IBin> Bins { get; }
        public bool IsSolved { get; }
        public IEnumerable<IBinsConfiguration> ChildConfigurations { get; }
        public IEnumerable<IBinsConfiguration> FatherConfigurations { get; }
        public int TreeDepth { get; }
        public bool AddFather(IBinsConfiguration father);
        public bool AddChild(IBinsConfiguration child);
        public bool RemoveFather(IBinsConfiguration father);
        public bool RemoveChild(IBinsConfiguration child);
        public IEnumerable<IBinsConfiguration> GenerateNextConfigurations(bool force = false);
        public void Add(IBin bin);
        public void Canonicalize();
        public string ToString(bool canonical);
    }
}
