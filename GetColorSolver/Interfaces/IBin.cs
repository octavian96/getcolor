﻿namespace GetColorSolver.Interfaces
{
    public interface IBin
    {
        public int Capacity { get; }
        public IEnumerable<Color> Content { get; }
        public int Level { get; }
        public int Available { get; }
        public Color TopColor { get; }
        public int TopColorsCount { get; }
        public bool IsFull { get; }
        public bool IsComplete { get; }
        public Color ContentAt(int level);
        public IEnumerable<Color> PopTopColors(int amount);
        public IEnumerable<Color> PeekTopColors(int amount);
        public IEnumerable<Color> PeekContent(int amount);
        public bool CanAdd(IEnumerable<Color> colors);
        public bool TryAdd(IEnumerable<Color> colors);
    }
}
