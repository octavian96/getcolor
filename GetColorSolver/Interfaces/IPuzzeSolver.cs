﻿namespace GetColorSolver.Interfaces
{
    public interface IPuzzleSolver
    {
        public IBinsConfiguration InitialConfiguration { get; }
        public bool IsSolved { get; }
        public IEnumerable<IBinsConfiguration> SolvingConfigurations { get; }
        public void Solve();
        public IEnumerable<IBinsConfiguration> ConfigurationPath(IBinsConfiguration configuration);
        public string ConfigurationPathString(IBinsConfiguration configuration);
        public IEnumerable<IBinsConfiguration> SolutionPath();
        public string SolutionPathString();
    }
}
