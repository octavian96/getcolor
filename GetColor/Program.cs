﻿// See https://aka.ms/new-console-template for more information
using GetColor;
using GetColorSolver.Interfaces;

Console.WriteLine("Generating initial configuration...");
IBinsConfiguration initialConfiguration = PuzzleFactory.ConfigurationFactory(
    4,
    new Color[][]{
        new Color[]{Color.Purple, Color.Blue, Color.Pink, Color.Yellow},
        new Color[]{Color.Purple, Color.Blue, Color.Yellow, Color.Pink},
        new Color[]{Color.Orange, Color.Pink, Color.Orange, Color.Purple},
        new Color[]{Color.Blue, Color.Orange, Color.Purple, Color.Pink},
        new Color[]{Color.Blue, Color.Orange, Color.Yellow, Color.Yellow},
        Array.Empty<Color>(),
        Array.Empty<Color>()
    });
//IBinsConfiguration initialConfiguration = PuzzleFactory.ConfigurationFactory(
//    4,
//    new Color[][]{
//        new Color[]{Color.Purple, Color.Purple, Color.Yellow, Color.Yellow},
//        new Color[]{Color.Purple, Color.Purple, Color.Yellow, Color.Yellow},
//        Array.Empty<Color>()
//    });
/*IBinsConfiguration initialConfiguration = PuzzleFactory.ConfigurationFactory(
    4,
    new Color[][]{
        new Color[]{Color.Purple, Color.Blue, Color.Purple, Color.Blue},
        new Color[]{Color.Purple, Color.Blue, Color.Purple, Color.Blue},
        Array.Empty<Color>()
    });*/
Console.WriteLine("Initial configuration:");
Console.WriteLine(initialConfiguration.ToString() + '\n');
Console.WriteLine("Creating puzzle...");
IPuzzleSolver puzzle = PuzzleFactory.DepthFirstPuzzle(initialConfiguration);
Console.WriteLine("Solving puzzle...");
puzzle.Solve();
if (puzzle.IsSolved)
{
    Console.WriteLine("Puzzle solved.");
    Console.WriteLine(puzzle.SolutionPathString());

    using StreamWriter outputFile = new("solution.html");
    outputFile.Write(puzzle.ToHTML());
}
else
    Console.WriteLine("Could not solve puzzle.");