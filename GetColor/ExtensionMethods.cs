﻿using GetColorSolver.Interfaces;
using System.Text;

namespace GetColor
{
    internal static class ExtensionMethods
    {
        public static string ToHTML(this IBin bin)
        {
            string htmlColorTemplate = @"
<div 
                class=""liquid middle""
                style=""
                    background: COLOR;
                    filter: drop-shadow(0 0 80px COLOR);
                    top: TOP%;
                    bottom: BOTTOM%;""></div>
";

            var html = new StringBuilder();
            html.Append(@"<div class=""bowl"">");

            int percentageHeight = 100 / bin.Capacity;
            for (int i = 0; i < bin.Level; ++i)
            {
                var color = bin.ContentAt(i);
                string colorTemplate = htmlColorTemplate.Replace("COLOR", color.ToString());
                colorTemplate = colorTemplate.Replace("BOTTOM", (i * percentageHeight).ToString());
                colorTemplate = colorTemplate.Replace("TOP", (100 - (i + 1) * percentageHeight).ToString());

                if (i == 0)
                    colorTemplate = colorTemplate.Replace("middle", "bottom");

                html.Append(colorTemplate);
            }

            html.Append("</div>");
            return html.ToString();
        }

        public static string ToHTML(this IBinsConfiguration configuration)
        {
            var html = new StringBuilder();
            html.Append(@"<div class=""container"">");

            foreach (var bottle in configuration.Bins)
                html.Append(bottle.ToHTML());

            html.Append("</div>");
            return html.ToString();
        }

        public static string ToHTML(this IPuzzleSolver puzzle)
        {
            if (!puzzle.IsSolved)
                return string.Empty;
            var html = new StringBuilder();
            string head = @"
<!DOCTYPE html>

<head>
    <style type=""text/css"">
        body {
            background-image: linear-gradient(180deg, rgb(5, 39, 103) 0%, #3a0647 70%);
        }

        .container {
            display: flex;
            height: 100%;
            flex-direction: row;
            flex-wrap: nowrap;
            align-items: center;
            justify-content: space-around;
            overflow: auto;
            -ms-overflow-style: none;
            /* IE and Edge */
            scrollbar-width: none;
            /* Firefox */

            /* Hide the scrollbar */
            &::-webkit-scrollbar {
                display: none;
                /* Chrome, Safari, and Opera */
            }
        }

        .bowl {
            position: relative;
            z-index: 0;
            min-width: 5rem;
            min-height: min-content;
            aspect-ratio: 1/4;
            margin: 1rem;
            background: rgba(255, 255, 255, 0.1);
            border-top-right-radius: 1000% 15rem;
            border-top-left-radius: 1000% 15rem;
            border-bottom-left-radius: 50rem;
            border-bottom-right-radius: 50rem;
            border: 0.5rem solid transparent;
        }

        .bowl::before {
            content: '';
            z-index: 1;
            position: absolute;
            top: -14px;
            left: 50%;
            transform: translateX(-50%);
            width: 110%;
            height: 20px;
            border: 6px solid;
            border-color: rgba(255, 255, 255, 0.15);
            border-radius: 50%;
            background-color: transparent;
            /*box-shadow: 0 5px rgba(255, 255, 255, 0.3);*/
        }

        .liquid {
            position: absolute;
            min-height: 50px;
            z-index: 0;
            left: 0.001rem;
            right: 0.001rem;
        }

        .liquid::before {
            content: '';
            position: absolute;
            top: -10px;
            width: 100%;
            height: 20px;
            background: inherit;
            filter: brightness(0.85);
            border-radius: 50%;
        }

        .middle::after {
            content: '';
            position: absolute;
            bottom: -10px;
            width: 100%;
            height: 20px;
            background: inherit;
            border-radius: 50%;
        }

        .top::before {
            filter: brightness(1);
        }

        .bottom {
            border-bottom-left-radius: 50rem;
            border-bottom-right-radius: 50rem;
        }
    </style>
</head>
";

            html.Append(head);
            html.Append("<body>");
            var path = puzzle.SolutionPath();
            foreach (var configuration in path)
                html.Append(configuration.ToHTML());

            html.Append("</body>");
            return html.ToString();
        }

    }
}
